#!/usr/bin/env python
'''
    METGLOBAL Talent Tutorial Project 1 : Phone Index Project
        Created by Hakan DEGIRMEN. hakan.degirmen@metglobal.com
        Date: Oct 3rd 2016 Monday
'''

import pickle  # for save pickle
import pdb  # for debug

# MACRO VARIABLES
PICKLE_NAME = 'save.p'
WRITABLE = 'wb'
READABLE = 'rb'


class PhoneNumber:
    '''
    PhoneNumber Class data members:
        c_code: country c_code
        op_code: operator/region c_code
        number: number
    '''
    def __init__(self, p_number=''):
        if len(p_number) == 13:
            self.c_code = p_number[0:3]
            self.op_code = p_number[3:6]
            self.number = p_number[6:]
        elif len(p_number) == 11:
            self.c_code = '+90'
            self.op_code = p_number[1:4]
            self.number = p_number[4:]
        elif len(p_number) == 10:
            self.c_code = '+90'
            self.op_code = p_number[0:3]
            self.number = p_number[3:]
        else:
            self.c_code = '+90'
            self.op_code = '000'
            self.number = p_number

    def __str__(self):
        return ''+self.c_code+' ('+self.op_code+') '+self.number

    def is_equal(self, other_object):

        if (self.c_code == other_object.c_code and
            self.op_code == other_object.op_code and
                self.number == other_object.number):
                return True
        return False

    def get_object(self):
        return {'c_code': self.c_code, 'op_code': self.op_code,
                'number': self.number}


class Person:

    def __init__(self, name='', surname=''):
        self.name = name
        self.surname = surname

    def __str__(self):
        return ''+self.get_object()['Name']+' '+self.get_object()['Surname']

    def is_equal(self, other_object):
        if (self.name == other_object.name and
                self.surname == other_object.surname):
                    return True
        return False

    def get_object(self):
        return {'Name': self.name, 'Surname': self.surname}


class EntryPhoneNumber:
    phone_number = PhoneNumber()
    person = Person()

    def __init__(self, phone_number=PhoneNumber(), person=Person()):
        self.phone_number = PhoneNumber()
        self.person = Person()
        self.person = person
        self.phone_number = phone_number

    def __str__(self):
        return self.get_object()['Name']+' '+self.get_object()['Surname']+' '\
                + self.get_object()['PhoneNumber']

    def get_object(self):
        return {'Name': self.person.name, 'Surname': self.person.surname,
                'PhoneNumber': str(self.phone_number)}

    def is_equal(self, other_object):
        if (self.person.is_equal(other_object.person) and
                self.phone_number.is_equal(other_object.phone_number)):
                    return True
        return False


class PhoneIndexApplication:
    entry_list = []

    def menu(self):
        print 'Welcome to Phone Index application.\n'
        request = ''

        while (request.upper() != "QUIT"):
            message = 'Plase enter what you want operation. '
            message += 'Add, List, Search or Quit'
            print message
            request = raw_input()
            if request.upper() == 'ADD':
                self.add_object()
            elif request.upper() == 'LIST':
                self.write_list(self.entry_list)
            elif request.upper() == 'SEARCH':
                result_list = self.search_operation()
                self.write_list(result_list)

            elif request.upper() == 'QUIT':
                break
            else:
                print 'You entered invalid operation !!!'

    def add_object(self):
        # Get data from user.
        print 'Enter Name: '
        name = raw_input()
        print 'Enter Surname'
        surname = raw_input()
        print 'Enter Phone Number'
        phone_number = raw_input()
        person_obj = Person(name, surname)
        phone_number_obj = PhoneNumber(phone_number)
        # Create entry
        entry = EntryPhoneNumber(phone_number_obj, person_obj)
        # Insert entry
        self.entry_list.append(entry)

        # Write file
        fileObject = open(PICKLE_NAME, WRITABLE)
        pickle.dump(entry, fileObject)
        fileObject.close()

    def write_list(self, list_obj):
        if len(list_obj) == 0:
            print 'List is empty\n'
        else:
            for entry in list_obj:
                print entry

    def search_operation(self):
        return self.search_object(self.get_target())

    def get_target(self):
        print 'Enter the target key'
        target = raw_input()
        return target

    def search_object(self, target):
        result_list = []
        for entry in self.entry_list:
            if (entry.person.name == target or entry.person.surname == target
                or PhoneNumber.
                    is_equal(entry.phone_number, PhoneNumber(target))):
                result_list.append(entry)
        return result_list


app1 = PhoneIndexApplication()
app1.menu()
