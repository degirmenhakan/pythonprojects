#!/usr/bin/env python
'''
    METGLOBAL Talent Tutorial Project 2 : Phone Index Project v2
        Created by Hakan DEGIRMEN. hakan.degirmen@metglobal.com
        Date: Oct 5rd 2016 Wednesday

    Project Goals:
        Learning:
            Database,
            Tables,
            SQL,
            Basic Authentication,
            Hashing
'''

'''
                        How Sqlite Storage Database:
                    =====================================
    SQLite is an embedded SQL database engine. Unlike most other SQL databases,
SQLite does not have a separate server process. SQLite reads and writes
directly to ordinary disk files. A complete SQL database with multiple tables,
indices, triggers, and views, is contained in a single disk file.

'''
import sqlite3  # for sqlite db library
import pdb  # for debug
import sys  # for error message
import hashlib

# MACRO VARIABLES
PICKLE_NAME = 'save.p'
WRITABLE = 'wb'
READABLE = 'rb'
DATABASENAME = 'project2.db'


class PhoneNumber:
    '''
    PhoneNumber Class data members:
        c_code: country c_code
        op_code: operator/region c_code
        number: number
    '''
    def __init__(self, p_number=''):
        if len(p_number) >= 13:
            self.c_code = p_number[0:3]
            self.op_code = p_number[3:6]
            self.number = p_number[6:]
        elif len(p_number) == 11:
            self.c_code = '+90'
            self.op_code = p_number[1:4]
            self.number = p_number[4:]
        elif len(p_number) == 10:
            self.c_code = '+90'
            self.op_code = p_number[0:3]
            self.number = p_number[3:]
        else:
            self.c_code = '+90'
            self.op_code = '000'
            self.number = p_number

    def __str__(self):
        return ''+self.c_code+' ('+self.op_code+') '+self.number

    def str_no_format(self):
        return self.c_code+''+self.op_code+''+self.number

    def str_format(self):
        return ''+self.c_code+' ('+self.op_code+') '+self.number

    def is_equal(self, other_object):

        if (self.c_code == other_object.c_code and
            self.op_code == other_object.op_code and
                self.number == other_object.number):
                return True
        return False

    def get_object(self):
        return {'c_code': self.c_code, 'op_code': self.op_code,
                'number': self.number}


class Person:

    def __init__(self, name='', surname=''):
        self.name = name
        self.surname = surname

    def __str__(self):
        return ''+self.get_object()['Name']+' '+self.get_object()['Surname']

    def is_equal(self, other_object):
        if (self.name == other_object.name and
                self.surname == other_object.surname):
                    return True
        return False

    def get_object(self):
        return {'Name': self.name, 'Surname': self.surname}


class EntryPhoneNumber:
    phone_number = PhoneNumber()
    person = Person()

    def __init__(self, phone_number=PhoneNumber(), person=Person()):
        self.phone_number = PhoneNumber()
        self.person = Person()
        self.person = person
        self.phone_number = phone_number

    def __str__(self):
        return self.get_object()['Name']+' '+self.get_object()['Surname']+' '\
                + self.get_object()['PhoneNumber']

    def get_object(self):
        return {'Name': self.person.name, 'Surname': self.person.surname,
                'PhoneNumber': str(self.phone_number)}

    def is_equal(self, other_object):
        if (self.person.is_equal(other_object.person) and
                self.phone_number.is_equal(other_object.phone_number)):
                    return True
        return False


class DBUser:
    username = ''
    password = ''
    is_readable = False
    is_writable = False

    def __init__(self, username='', password='', is_readable=False,
                 is_writable=False):
        self.username = username
        self.password = password
        self.is_readable = is_readable
        self.is_writable = is_writable


class PhoneIndexApplication:
    entry_list = []
    db_helper_obj = None
    is_login = False

    def __init__(self):
        self.db_helper_obj = DBHelper(DATABASENAME)

        # create database user table
        query = 'CREATE TABLE IF NOT EXISTS tbl_User('
        query += 'ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,'
        query += 'USERNAME  TEXT   NOT NULL, PASSWORD TEXT    NOT NULL, '
        query += 'ISREADABLE BOOLEAN NOT NULL, ISWRITEABLE BOOLEAN NOT NULL );'
        self.db_helper_obj.create_table(query)

        # create phone index TABLE
        query = 'Create table IF NOT EXISTS tbl_Phone_Index ('
        query += 'ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,'
        query += 'NAME  TEXT    NOT NULL, SURNAME TEXT    NOT NULL, '
        query += 'PHONENUMBER  TEXT    NOT NULL );'
        self.db_helper_obj.create_table(query)

    def menu(self, db_user):
        print 'Welcome to Phone Index application.\n'
        request = ''

        while (request.upper() != "QUIT"):
            message = 'Plase enter what you want operation. '
            message += 'Add, List, Search or Quit'
            print message
            request = raw_input()
            if request.upper() == 'ADD':
                if db_user.is_writable:
                    self.add_object()
                else:
                    print 'Permission denided. You can not add new object'
            elif request.upper() == 'LIST':
                if db_user.is_readable:
                    entry_list = self.get_list()
                    self.write_list(entry_list)
                else:
                    print 'Permission denided. You can not access table '
            elif request.upper() == 'SEARCH':
                if db_user.is_readable:
                    self.write_list(self.search_operation())
                else:
                    print 'Permission denided. You can not access table '
            elif request.upper() == 'QUIT':
                break
            else:
                print 'You entered invalid operation !!!'

    def get_list(self):
        entry_list = []
        query = 'Select * from tbl_Phone_Index'
        rows = self.db_helper_obj.select(query)
        for row in rows:
            person_obj = Person()
            person_obj.name = row[1]
            person_obj.surname = row[2]
            phone_number_obj = PhoneNumber(row[3])
            entry = EntryPhoneNumber(phone_number_obj, person_obj)
            entry_list.append(entry)
        return entry_list

    def add_object(self):
        # Get data from user.
        print 'Enter Name: '
        name = raw_input()
        print 'Enter Surname'
        surname = raw_input()
        print 'Enter Phone Number'
        phone_number = raw_input()
        person_obj = Person(name, surname)
        phone_number_obj = PhoneNumber(phone_number)
        # Create entry
        entry = EntryPhoneNumber(phone_number_obj, person_obj)

        # create query
        query = "Insert into tbl_Phone_Index (NAME, SURNAME, PHONENUMBER )"
        query += "VALUES ('"+entry.person.name+"', '"+entry.person.surname
        query += "','"+entry.phone_number.str_no_format()+"') "
        # Insert entry
        self.db_helper_obj.insert(query)

    # The function checks what not same username
    def check_username(self, u_name):
        query = "Select * from tbl_User where USERNAME = '"+u_name+"''"
        rows = self.db_helper_obj.select(query)
        # check anybody use the username
        if len(rows) == 0:
            return True
        else:
            return False

    def write_list(self, list_obj):
        if len(list_obj) == 0:
            print 'List is empty\n'
        else:
            for entry in list_obj:
                print entry

    def search_operation(self):
        return self.search_object(self.get_target())

    # for search key
    def get_target(self):
        print 'Enter the target key:'
        target = raw_input()
        return target

    def search_object(self, target):
        result_list = []
        query = "Select * from tbl_Phone_Index where NAME= '"+target
        query += "' or SURNAME = '"+target+"' or PHONENUMBER = '"+target+"'"
        rows = self.db_helper_obj.select(query)
        for row in rows:
            entry = EntryPhoneNumber(PhoneNumber(row[3]),
                                     Person(row[1], row[2]))
            result_list.append(entry)
        return result_list

    def login(self):
        login_user = DBUser()
        # if has any database user
        if len(self.db_helper_obj.select('select * from tbl_User')) == 0:
            login_user = self.add_new_user()
        else:
            username = raw_input('Enter the username: ')
            password = raw_input('Enter the password: ')

            query = "Select * from tbl_User where USERNAME = '"
            query += username+"' and "
            query += " PASSWORD = '"+self.crypt(password)+"'"
            rows = self.db_helper_obj.select(query)
            # if not found database user
            if len(rows) == 0:
                print 'Incorrect username or password.'
                self.is_login = False
            else:
                print ''+rows[0][1]
                login_user.username = rows[0][1]
                login_user.is_readable = rows[0][3]
                login_user.is_writable = rows[0][4]
                self.is_login = True

        return login_user

    def add_new_user(self):

        # for control match password and confirm password
        is_done = False

        # get data for new user object
        while not is_done:
            username = raw_input('Enter the username: ')
            password = raw_input('Enter the password: ')
            confirm_password = raw_input('Re Enter the password: ')
            is_readable = raw_input('Is readable : True or False: ')
            is_writable = raw_input('Is write : True or False: ')

            if password == confirm_password:
                is_done = True
            else:
                print 'No match password and confirm password. Plase retry.'
                is_done = False

        # Create new dbuser object
        new_user = DBUser(username, password)
        if is_readable.upper() == 'TRUE':
            new_user.is_readable = True
        if is_writable.upper() == 'TRUE':
            new_user.is_writable = True

        # create query
        query = 'INSERT INTO tbl_User(USERNAME,PASSWORD,ISREADABLE,'
        query += 'ISWRITEABLE)'
        query += "VALUES('"+new_user.username+"','"
        query += self.crypt(new_user.password)+"','"
        query += str(new_user.is_readable)+"','"+str(new_user.is_writable)+"')"

        # insert new database user
        self.db_helper_obj.insert(query)
        self.is_login = True
        return new_user

    def crypt(self, key):
        return hashlib.md5(key).hexdigest()

    def start(self):
        db_user = DBUser()
        db_user = self.login()
        if self.is_login:
            self.menu(db_user)
        else:
            print 'Plase log in'


class DBHelper:

    connection = None
    db_name = ''

    def __init__(self, db_name):
        self.db_name = db_name

    def get_connection(self):
        try:
            self.connection = sqlite3.connect(self.db_name)
            return self.connection
        except sqlite3.Error, e:
            sys.stderr.write(e.args[0])
            sys.exit(1)
            return None

    def create_table(self, query):
        try:
            if self.get_connection() != None:
                cursor = self.get_connection().cursor()
                cursor.execute(query)
            else:
                sys.stderr.write('Can not connect db %s' % self.db_name)
        except sqlite3.Error, e:
            sys.stderr.write(e.args[0])
        finally:
            if self.connection:
                self.connection.close()

    def insert(self, query):
        try:
            if self.get_connection() != None:
                cursor = self.get_connection().cursor()
                cursor.execute(query)
                self.connection.commit()
            else:
                sys.stderr.write('Can not connect db %s' % self.db_name)

        except sqlite3.Error, e:
            sys.stderr.write(e.args[0])

        finally:
            if self.connection:
                self.connection.close()

    def select(self, query):

        rows = []
        try:
            if self.get_connection() != None:
                cursor = self.get_connection().cursor()
                cursor.execute(query)
                rows = cursor.fetchall()
                self.connection.close()
                return rows
            else:
                sys.stderr.write('Can not connect db %s' % self.db_name)

        except sqlite3.Error, e:
            sys.stderr.write(e.args[0])
        finally:
            if self.connection:
                self.connection.close()
            return rows


app1 = PhoneIndexApplication()
app1.start()
